#pragma once

/*
* Jump to a machine code routine at NNN
* This instruction is only used on the old computers which Chip-8 was originally implemented. 
* It is ignored by modern interpreters
*/
void fn0NNN(void**);

/*
* Clear the display
*/
void fn00E0(void**);

/*
* Return from a subroutine.
*/
void fn00EE(void**);

/*
* 1NNN: Jumps to address NNN
*/
void fn1NNN(void**);

/*
* 2NNN: Calls subroutine at NNN
*/
void fn2NNN(void**);

/*
* 3XNN: Skips the next instruction if VX equals NN
*/
void fn3XNN(void**);

/*
* 4XNN: Skips the next instruction if VX doesn't equal NN
*/
void fn4XNN(void**);

/*
* 5XY0: Skips the next instruction if VX equals VY
*/
void fn5XY0(void**);

/*
* 6XNN: Sets VX to NN
*/
void fn6XNN(void**);

/*
* 7XNN: Adds NN to VX
*/
void fn7XNN(void**);

/*
* 8XY0: Sets VX to the value of VY
*/
void fn8XY0(void**);

/*
* 8XY1: Sets VX to VX or VY
*/
void fn8XY1(void**);

/*
* 8XY2: Sets VX to VX and VY
*/
void fn8XY2(void**);

/*
* 8XY3: Sets VX to VX xor VY
*/
void fn8XY3(void**);

/*
* 8XY4: Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't
*/
void fn8XY4(void**);

/*
* 8XY5: VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't
*/
void fn8XY5(void**);

/*
* 8XY6: Shifts VX right by one. VF is set to the value of the least significant bit of VX before the shift
*/
void fn8XY6(void**);

/*
* 8XY7: Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't
*/
void fn8XY7(void**);

/*
* 8XYE: Shfits VX left by one. VF is set to the value of the most significant bit of VX before the shift
*/
void fn8XYE(void**);

/*
* 9XY0: Skips the next instruction if VX doesn't equal VY
*/
void fn9XY0(void**);

/*
* ANNN: Sets I to the address NNN
*/
void fnANNN(void**);

/*
* BNNN: Jumps to the address NNN plus V0
*/
void fnBNNN(void**);

/*
* CXNN: Sets VX to a random number and NN
*/
void fnCXNN(void**);

/*
* DXYN: Sprites stored in memory at location in index register(I), maximum 8 bits wide
		 Wraps around the screen. If when drawn, clears a pixel, register VF is set to 1 otherwise it is zero
		 All drawing is XOR drawing (it toggles the screen pixels)
*/
void fnDXYN(void**);

/*
* EX9E: Skips the next instruction if the key stored in VX is pressed
*/
void fnEX9E(void**);

/*
* EXA1: Skips the next instruction if the key stored in VX isn't pressed
*/
void fnEXA1(void**);

/*
* FX07: Sets VX to the value of the delay timer
*/
void fnFX07(void**);

/*
* FX0A: A key press is awaited, and then stored in VX
*/
void fnFX0A(void**);

/*
* FX15: Sets the delay timer to VX
*/
void fnFX15(void**);

/*
* FX18: Sets the sound timer to VX
*/
void fnFX18(void**);

/*
* FX1E: Adds VX to I
*/
void fnFX1E(void**);

/*
* FX29: Sets I to the location of the sprite for the character in VX
		 Characters 0-F are represented by a 4x5 font
*/
void fnFX29(void**);

/*
* FX33: Stores the Binary-coded decimal representation of VX,
		 with the most significant of three digits at the address in I, the middle digit at I plus 1,
		 and the least significant digit at I plus 2 (take the decimal representation of VX,
		 place the hundreds digit in memory at location in I
		 the tens digit at location I + 1, and the ones digit at location I+2
*/
void fnFX33(void**);

/*
* FX55: Stores V0 to VX in memory starting at address I
*/
void fnFX55(void**);

/*
* FX65: Fills V0 to VX with values from memory starting at address I
*/
void fnFX65(void**);

