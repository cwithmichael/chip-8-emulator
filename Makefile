CC=gcc
CFLAGS= -g -std=c99
LIBS= -lGL -lGLU -lglut -lGLEW

SRC = chip8.c opcodes.c cpu.c

OBJS = $(SRC:.c=.o)

all: fun

%.o: %.c
	$(CC) -c -o $(@F) $(CFLAGS) $<

fun: $(OBJS)
	$(CC) -o $(@F)  $(OBJS) $(LIBS)

clean:
	rm -f $(OBJS)
	rm -f fun
