#pragma once

#include <stdint.h>
#define NUM_REGS 0x12
#define MAX_MEM 0xFFF
#define GFX_SIZE 64*32
#define KEY_SIZE 0x10
#define STACK_SIZE 0x10
#define MEM_SIZE 0xFFF
#define OPCODE_SIZE 4
#define FONT_SIZE 80

enum {V0, V1, V2, V3, V4, V5, V6, V7, V8, V9, VA, VB, VC, VD, VE, VF};

typedef struct CpuContext_ {
	int cycles;
	uint8_t mem[MEM_SIZE];
	uint8_t gfx[GFX_SIZE];
	uint8_t keys[KEY_SIZE];
	uint16_t stack[STACK_SIZE];
	uint8_t delay_timer;
	uint8_t sound_timer;
	uint8_t regs[NUM_REGS];
	uint16_t PC;
	uint16_t SP;
	uint16_t I;
	uint16_t opcode;
	uint16_t locx;
	uint16_t locy;
	uint16_t height;
} CpuContext;

extern struct CpuContext_ cpu;
extern int8_t DRAW;
extern FILE *log_file;


void decode();
void init();
void fetch();
void cycle();
