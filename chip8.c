/*Emulator for the Chip8 System. I used the graphics impementation from the MultiGesture tutorial but everything else I pretty much did own my own. */
#include <signal.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include "cpu.h"

#define false 0
#define true 1
#define SCREEN_WIDTH 64
#define SCREEN_HEIGHT 32
#define MODIFIER 10

uint8_t screenData[SCREEN_HEIGHT][SCREEN_WIDTH][3]; 
void setupTexture();
void updateTexture();
void reshape_window(GLsizei w, GLsizei h);
void keyboardDown(unsigned char key, int x, int y);
void keyboardUp(unsigned char key, int x, int y);
// Window size
int display_width = SCREEN_WIDTH * MODIFIER;
int display_height = SCREEN_HEIGHT * MODIFIER;


void onDisplay()
{
	cycle();
	if(DRAW){
		glClear(GL_COLOR_BUFFER_BIT);
		updateTexture();
		glutSwapBuffers();
		DRAW = false;
	}
	glutPostRedisplay();
}

int main(int argc, char **argv)
{
	srand(time(NULL));
	if(argc < 2) exit(1);
	char *game_name = argv[1];
	FILE *game = fopen(game_name, "rb");
  	init();
	if (game != NULL) {
		fseek(game, 0, SEEK_END);
		uint16_t game_size = ftell(game);
		uint8_t game_buffer[game_size];
		fseek(game, 0, SEEK_SET);
                int result = fread(&game_buffer, sizeof(*game_buffer), game_size, game);
		if(result == game_size) {
			int i;
			for (i=0; i<game_size; i++) {
				cpu.mem[i + 0x200] = game_buffer[i];
			}
			printf("You're about to play %s\n", game_name);
			printf("Size: %d\n", game_size);
			
		}
		
	}
	
	else {
		printf("Couldn't open file\n");
		exit(0);
	}

	/* Glut-related initialising functions */
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE);
	glutInitWindowSize(display_width, display_height);
	glutInitWindowPosition(320, 320);
	glutCreateWindow("Chip8 Emulator");

	/* We can display it if everything goes OK */
	glutDisplayFunc(onDisplay);
	//glutIdleFunc(onDisplay);
	glutReshapeFunc(reshape_window);        
	glutKeyboardFunc(keyboardDown);
	glutKeyboardUpFunc(keyboardUp); 
	setupTexture();			
	glutMainLoop();
	fclose(log_file);
  	fclose(game);
	return 0;
}

// Setup Texture
void setupTexture()
{
	// Clear screen
	for(int y = 0; y < SCREEN_HEIGHT; ++y)		
		for(int x = 0; x < SCREEN_WIDTH; ++x)
			screenData[y][x][0] = screenData[y][x][1] = screenData[y][x][2] = 0;

	// Create a texture 
	glTexImage2D(GL_TEXTURE_2D, 0, 3, SCREEN_WIDTH, SCREEN_HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, (GLvoid*)screenData);

	// Set up the texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP); 

	// Enable textures
	glEnable(GL_TEXTURE_2D);
}

void updateTexture()
{	
	// Update pixels
	for(int y = 0; y < 32; ++y)		
		for(int x = 0; x < 64; ++x)
			if(cpu.gfx[(y * 64) + x] == 0)
				screenData[y][x][0] = screenData[y][x][1] = screenData[y][x][2] = 0;	// Disabled
			else 
				screenData[y][x][0] = screenData[y][x][1] = screenData[y][x][2] = 255;  // Enabled
		
	// Update Texture
	glTexSubImage2D(GL_TEXTURE_2D, 0 ,0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, GL_RGB, GL_UNSIGNED_BYTE, (GLvoid*)screenData);

	glBegin( GL_QUADS );
	glTexCoord2d(0.0, 0.0);	glVertex2d(0.0, 0.0);
	glTexCoord2d(1.0, 0.0);	glVertex2d(display_width, 0.0);
	glTexCoord2d(1.0, 1.0);	glVertex2d(display_width, display_height);
	glTexCoord2d(0.0, 1.0);	glVertex2d(0.0, display_height);
	glEnd();
}

void reshape_window(GLsizei w, GLsizei h)
{
	glClearColor(0.0f, 0.0f, 0.5f, 0.0f);
	glMatrixMode(GL_PROJECTION);
  	glLoadIdentity();
  	gluOrtho2D(0, w, h, 0);        
  	glMatrixMode(GL_MODELVIEW);
  	glViewport(0, 0, w, h);

	// Resize quad
	display_width = w;
	display_height = h;
}

void keyboardDown(unsigned char key, int x, int y)
{
	if(key == 27)    // esc
		exit(0);

	if(key == '1')		cpu.keys[0x1] = 1;
	else if(key == '2')	cpu.keys[0x2] = 1;
	else if(key == '3')	cpu.keys[0x3] = 1;
	else if(key == '4')	cpu.keys[0xC] = 1;

	else if(key == 'q')	cpu.keys[0x4] = 1;
	else if(key == 'w')	cpu.keys[0x5] = 1;
	else if(key == 'e')	cpu.keys[0x6] = 1;
	else if(key == 'r')	cpu.keys[0xD] = 1;

	else if(key == 'a')	cpu.keys[0x7] = 1;
	else if(key == 's')	cpu.keys[0x8] = 1;
	else if(key == 'd')	cpu.keys[0x9] = 1;
	else if(key == 'f')	cpu.keys[0xE] = 1;

	else if(key == 'z')	cpu.keys[0xA] = 1;
	else if(key == 'x')	cpu.keys[0x0] = 1;
	else if(key == 'c')	cpu.keys[0xB] = 1;
	else if(key == 'v')	cpu.keys[0xF] = 1;

	//printf("Press key %c\n", key);
}

void keyboardUp(unsigned char key, int x, int y)
{
	if(key == '1')		cpu.keys[0x1] = 0;
	else if(key == '2')	cpu.keys[0x2] = 0;
	else if(key == '3')	cpu.keys[0x3] = 0;
	else if(key == '4')	cpu.keys[0xC] = 0;

	else if(key == 'q')	cpu.keys[0x4] = 0;
	else if(key == 'w')	cpu.keys[0x5] = 0;
	else if(key == 'e')	cpu.keys[0x6] = 0;
	else if(key == 'r')	cpu.keys[0xD] = 0;

	else if(key == 'a')	cpu.keys[0x7] = 0;
	else if(key == 's')	cpu.keys[0x8] = 0;
	else if(key == 'd')	cpu.keys[0x9] = 0;
	else if(key == 'f')	cpu.keys[0xE] = 0;

	else if(key == 'z')	cpu.keys[0xA] = 0;
	else if(key == 'x')	cpu.keys[0x0] = 0;
	else if(key == 'c')	cpu.keys[0xB] = 0;
	else if(key == 'v')	cpu.keys[0xF] = 0;
}

