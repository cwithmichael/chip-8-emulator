#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include "cpu.h"
#include "opcodes.h"

struct CpuContext_ cpu;
int8_t DRAW;
FILE *log_file;

void init()
{
	log_file = fopen("opcodes_log", "w");
	cpu.PC = 0x200;
	uint8_t fontset[FONT_SIZE] = {
        0xF0, 0x90, 0x90, 0x90, 0xF0, //0
        0x20, 0x60, 0x20, 0x20, 0x70, //1
        0xF0, 0x10, 0xF0, 0x80, 0xF0, //2
        0xF0, 0x10, 0xF0, 0x10, 0xF0, //3
        0x90, 0x90, 0xF0, 0x10, 0x10, //4
        0xF0, 0x80, 0xF0, 0x10, 0xF0, //5
        0xF0, 0x80, 0xF0, 0x90, 0xF0, //6
        0xF0, 0x10, 0x20, 0x40, 0x40, //7
        0xF0, 0x90, 0xF0, 0x90, 0xF0, //8
        0xF0, 0x90, 0xF0, 0x10, 0xF0, //9
        0xF0, 0x90, 0xF0, 0x90, 0x90, //A
        0xE0, 0x90, 0xE0, 0x90, 0xE0, //B
        0xF0, 0x80, 0x80, 0x80, 0xF0, //C
        0xE0, 0x90, 0x90, 0x90, 0xE0, //D
        0xF0, 0x80, 0xF0, 0x80, 0xF0, //E
        0xF0, 0x80, 0xF0, 0x80, 0x80 //F
	};
	cpu.I = 0;
	cpu.SP = 0;
	cpu.sound_timer = 0;
	cpu.delay_timer = 0;
	for(int i=0; i < MEM_SIZE; i++) cpu.mem[i] = 0;
	for(int i=0; i < STACK_SIZE; i++) cpu.stack[i] = 0;
	for(int i=0; i < GFX_SIZE; i++) cpu.gfx[i] = 0;
  	for(int i=0; i < KEY_SIZE; i++) cpu.keys[i] = 0;
	for(int i=0; i < FONT_SIZE; i++) cpu.mem[i] = fontset[i];
}

void fetch()
{
	if(log_file != NULL)
		fprintf(log_file, "Program Counter: %x\n", cpu.PC);
	cpu.opcode = cpu.mem[cpu.PC] << 8 | cpu.mem[cpu.PC + 1];
	if ( cpu.opcode == 0)
		exit(1);
	fprintf(log_file, "The opcode %x\n", cpu.opcode);
}

/**
* CHIP-8 has 35 opcodes, which are all two bytes long.
* The most significant byte is stored first.
* The opcodes are listed below, in hexadecimal and with the following symbols
* NNN: address
* NN: 8-bit constant
* N: 4-bit constant
* X and Y: 4-bit register identifier
**/
void decode()
{
	static void (* const pf[35])(void **) =
	   {fn0NNN, fn00E0, fn00EE, fn1NNN, fn2NNN, fn3XNN, fn4XNN, fn5XY0,
		fn6XNN, fn7XNN, fn8XY0, fn8XY1, fn8XY2, fn8XY3, fn8XY4, fn8XY5,
		fn8XY6, fn8XY7, fn8XYE, fn9XY0, fnANNN, fnBNNN, fnCXNN, fnDXYN,
		fnEX9E, fnEXA1, fnFX07, fnFX0A, fnFX15, fnFX18, fnFX1E, fnFX29,
		fnFX33, fnFX55, fnFX65};

	uint8_t X, Y, N;
	uint16_t nnn_addr=0, nn_addr=0, a=0, b=0, c=0;
	uint16_t **input = malloc(sizeof(uint16_t) * 3);
	input[0] = &a;
	input[1] = &b;
	input[2] = &c;
	uint8_t first_id = cpu.mem[cpu.PC]>>4;
	nnn_addr = cpu.opcode & 0x0FFF;
	nn_addr = cpu.opcode & 0x00FF;
	fprintf(log_file, "nnn_addr: %x, nn_addr: %x\n", nnn_addr, nn_addr);
	assert(nnn_addr <= MAX_MEM);
	assert(nn_addr <= 0xFF);
	X = (cpu.opcode & 0x0F00) >> 8;
	Y = (cpu.opcode & 0x00F0) >> 4;
	N = (cpu.opcode & 0x000F);
	switch(first_id) {
		case 0:
			if (nnn_addr == 0x0E0) {
			  (*pf[1])(NULL);
			}
			else if (nnn_addr == 0x0EE) {
			  (*pf[2])(NULL);
			}
			else if (nnn_addr <= 0x1FF) {
			  printf("incorrect opcode\n");
			}
			else {
        			*input[0] = nnn_addr;
			  	(*pf[0])((void**)input);
      			}
			break;
		case 1:
      			*input[0] = nnn_addr;
			(*pf[3])((void**)input);
			break;
		case 2:
      			*input[0] = nnn_addr;
			(*pf[4])((void**)input);
			break;
		case 3:
			*input[0] = X;
			*input[1] = nn_addr;
			(*pf[5])((void**)input);
			break;
		case 4:
			*input[0] = X;
			*input[1] = nn_addr;
			(*pf[6])((void**)input);
			break;
		case 5:
			*input[0] = X;
			*input[1] = Y;
			(*pf[7])((void**)input);
			break;
		case 6:
			*input[0] = X;
			*input[1] = nn_addr;
			(*pf[8])((void**)input);
			break;
		case 7:
			*input[0] = X;
			*input[1] = nn_addr;
			(*pf[9])((void**)input);
			break;
		case 8:
			*input[0] = X;
			*input[1] = Y;
			if (N == 0){
				(*pf[10])((void**)input);
			}
			if (N == 1){
				(*pf[11])((void**)input);
			}
			if (N== 2){
				(*pf[12])((void**)input);

			}
			if (N == 3){
				(*pf[13])((void**)input);
			}
			if (N == 4){
				(*pf[14])((void**)input);
			}
			if (N == 5){
				(*pf[15])((void**)input);
			}
			if (N == 6){
				(*pf[16])((void**)input);
			}
			if (N == 7){
				(*pf[17])((void**)input);
			}
			if (N == 0xE){
				(*pf[18])((void**)input);
			}
			break;
		case 9:
			*input[0] = X;
			*input[1] = Y;
			(*pf[19])((void**)input);
			break;
		case 0xa:
			*input[0] = nnn_addr;
			(*pf[20])((void**)input);
			break;
		case 0xb:
			*input[0] = nnn_addr;
			(*pf[21])((void**)input);
			break;
		case 0xc:
			*input[0] = X;
			*input[1] = nn_addr;
			(*pf[22])((void**)input);
			break;
		case 0xd:
			*input[0] = X;
			*input[1] = Y;
			*input[2] = N;
			(*pf[23])((void**)input);
			break;
		case 0xe:
			if (Y == 0x9) {
				*input[0] = X;
				(*pf[24])((void**)input);
			}
			if (Y == 0xa) {
				*input[0] = X;
				(*pf[25])((void**)input);
			}
			break;
		case 0xf:
			*input[0] = X;
			if (Y == 0x0 && N == 0x7) {
				(*pf[26])((void**)input);
			}
			if (Y == 0x0 && N == 0xa) {
				(*pf[27])((void**)input);
			}
			if (Y == 0x1 && N == 0x5) {
				(*pf[28])((void**)input);
			}
			if (Y == 0x1 && N == 0x8) {
				(*pf[29])((void**)input);
			}
			if (Y == 0x1 &&  N == 0xe) {
				(*pf[30])((void**)input);
			}
			if (Y == 0x2 && N == 0x9) {
				(*pf[31])((void**)input);
			}
			if (Y == 0x3 && N == 0x3) {
				(*pf[32])((void**)input);
			}
			if (Y == 0x5 && N == 0x5) {
				(*pf[33])((void**)input);
			}
			if (Y == 0x6 && N == 0x5) {
				(*pf[34])((void**)input);
			}
			break;
		default:
			printf("incorrect opcode -> rebooting system\n");
			init();
		}
	free(input);
}

void cycle()
{
	fetch();
	decode();
	cpu.cycles++;
  	if(cpu.delay_timer > 0)
    		--cpu.delay_timer;
  	if(cpu.sound_timer > 0) {
    		if(cpu.sound_timer == 1) {
      			printf("some type of sound\n");
      			--cpu.sound_timer;
    		}
  	}
	//printf("Cycle: %d\n", cpu.cycles);
}

