#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include "opcodes.h"
#include "cpu.h"


void fn0NNN(void **input)
{
  assert(input != NULL);
  fprintf(log_file, "calling rca 1802 program at: %x\n", *(uint16_t*)input);
  cpu.stack[cpu.SP] = cpu.PC;
  ++cpu.SP;
  cpu.PC = *(uint16_t*)input;
}

void fn00E0(void **input)
{
  assert(input == NULL);
  for(int i=0; i < GFX_SIZE; i++) cpu.gfx[i] = 0;
  DRAW = 1;
  fprintf(log_file, "screen cleared\n");
  cpu.PC += 2;
}

void fn00EE(void **input)
{
  assert(input == NULL);
  fprintf(log_file, "Returning from a subroutine\n");
  --cpu.SP;
  fprintf(log_file, "Now we go to %x\n", cpu.stack[cpu.SP]);
  cpu.PC = cpu.stack[cpu.SP];
  cpu.PC += 2;
} 

void fn1NNN(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  fprintf(log_file, "Jumping to address: %x\n", *input_array[0]);
  cpu.PC= *input_array[0];
}

void fn2NNN(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  fprintf(log_file, "Calling subroutine at address: %x\n", *input_array[0]);
  cpu.stack[cpu.SP] = cpu.PC;
  ++cpu.SP;
  cpu.PC = *input_array[0];
}

void fn3XNN(void **input)
{
  assert(input != NULL);
  uint8_t **input_array = (uint8_t**)input;
  if (cpu.regs[*input_array[0]] == *input_array[1]) {
    fprintf(log_file, "skipping next instruction\n");
    fprintf(log_file, "because %x is equal to %x\n", cpu.regs[*input_array[0]], *input_array[1]);
    cpu.PC += 4;
  }
  else {
    fprintf(log_file, "not skipping\n");
    fprintf(log_file, "because %x is not equal to %x\n", cpu.regs[*input_array[0]], *input_array[1]);
    cpu.PC += 2;
  }
}

void fn4XNN(void **input)
{
  assert(input != NULL);
  uint8_t **input_array = (uint8_t**)input;
  if (cpu.regs[*input_array[0]] != *input_array[1]) {
    fprintf(log_file, "skipping next instruction\n");
    fprintf(log_file, "because %x is not equal to %x\n", cpu.regs[*input_array[0]], *input_array[1]);
    cpu.PC += 4;
  }
  else {
    fprintf(log_file, "not skipping\n");
    fprintf(log_file, "because %x is equal to %x\n", cpu.regs[*input_array[0]], *input_array[1]);
    cpu.PC += 2;
  }
}

void fn5XY0(void **input)
{
  assert(input != NULL);
  uint8_t **input_array = (uint8_t**)input;
  if (cpu.regs[*input_array[0]] == cpu.regs[*input_array[1]]) {
    fprintf(log_file, "skipping next instruction\n");
    fprintf(log_file, "because V%x is equal to V%x\n", *input_array[0], *input_array[1]);
    cpu.PC += 4;
  }
  else {
    fprintf(log_file, "not skipping\n");
    fprintf(log_file, "because V%x is not equal to V%x\n", *input_array[0], *input_array[1]);
    cpu.PC += 2;
  }
}

void fn6XNN(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  fprintf(log_file, "Putting %x in V%x\n", *input_array[1], *input_array[0]);
  cpu.regs[*input_array[0]] = *input_array[1];
  cpu.PC += 2;
}

void fn7XNN(void **input)
{
  assert(input != NULL);
  uint8_t **input_array = (uint8_t**)input;
  fprintf(log_file, "Adding %x to %x\n", *input_array[1], cpu.regs[*input_array[0]]);
  cpu.regs[*input_array[0]] = cpu.regs[*input_array[0]] + *input_array[1];
  cpu.PC += 2;
}

void fn8XY0(void **input)
{
  assert(input != NULL);
  uint8_t **input_array = (uint8_t**)input;
  fprintf(log_file, "Setting VX to the value of VY: %x\n", *input_array[1]);
  cpu.regs[*input_array[0]] = cpu.regs[*input_array[1]];
  cpu.PC += 2;
}

void fn8XY1(void **input)
{
  assert(input != NULL);
  uint8_t **input_array = (uint8_t**)input;
  fprintf(log_file, "Setting VX to the value of VX|VY: %x\n", *input_array[0]|*input_array[1]);
  cpu.regs[*input_array[0]] = (cpu.regs[*input_array[0]]) | (cpu.regs[*input_array[1]]);
  cpu.PC += 2;
}

void fn8XY2(void **input)
{
  assert(input != NULL);
  uint8_t **input_array = (uint8_t**)input;
  fprintf(log_file, "Setting VX to the value of VX&VY: %x\n", *input_array[0]&*input_array[1]);
  cpu.regs[*input_array[0]] = (cpu.regs[*input_array[0]]) & (cpu.regs[*input_array[1]]);
  cpu.PC += 2;
}

void fn8XY3(void **input)
{
  assert(input != NULL);
  uint8_t **input_array = (uint8_t**)input;
  fprintf(log_file, "Setting VX to the value of VX^VY: %x\n", *input_array[0]^*input_array[1]);
  cpu.regs[*input_array[0]] = (cpu.regs[*input_array[0]]) ^ (cpu.regs[*input_array[1]]);
  cpu.PC += 2;
}

void fn8XY4(void **input)
{
  assert(input != NULL);
  uint8_t **input_array = (uint8_t**)input;
  fprintf(log_file, "Adding VY to VX and setting VF Flag\n");
  if ((cpu.regs[*input_array[1]]) > (0xFF - cpu.regs[*input_array[0]])) { 
    cpu.regs[VF] = 1;
  }
  else {
    cpu.regs[VF] = 0;
  }
  cpu.regs[*input_array[0]] = (cpu.regs[*input_array[0]]) + (cpu.regs[*input_array[1]]);
  cpu.PC += 2;
}

void fn8XY5(void **input)
{
  assert(input != NULL);
  uint8_t **input_array = (uint8_t**)input;
  fprintf(log_file, "Subtracting VY from VX and setting VF Flag\n");
  if (cpu.regs[*input_array[1]] > cpu.regs[*input_array[0]]) {
    cpu.regs[VF] = 0;
  }
  else {
    cpu.regs[VF] = 1;
  } 
  cpu.regs[*input_array[0]] = cpu.regs[*input_array[0]] - cpu.regs[*input_array[1]];
  cpu.PC += 2;
}

void fn8XY6(void **input)
{
  assert(input != NULL);
  uint8_t **input_array = (uint8_t**)input;
  fprintf(log_file, "Shifting VX to right by one\n");
  cpu.regs[VF] = cpu.regs[*input_array[0]] & 0x01;
  cpu.regs[*input_array[0]] = cpu.regs[*input_array[0]] >> 1;
  cpu.PC += 2;
}

void fn8XY7(void **input)
{
  assert(input != NULL);
  uint8_t **input_array = (uint8_t**)input;
  fprintf(log_file, "Subtracting VY from VX and setting VF Flag\n");
  if (cpu.regs[*input_array[0]] > cpu.regs[*input_array[1]]) {
    cpu.regs[VF] = 0;
  }
  else {
    cpu.regs[VF] = 1;
  } 
  cpu.regs[*input_array[0]] = cpu.regs[*input_array[1]] - cpu.regs[*input_array[0]];
  cpu.PC += 2;

}

void fn8XYE(void **input)
{
  assert(input != NULL);
  uint8_t **input_array = (uint8_t**)input;
  fprintf(log_file, "Shifting VX to left by one\n");
  cpu.regs[VF] = cpu.regs[*input_array[0]] >> 7;
  cpu.regs[*input_array[0]] = cpu.regs[*input_array[0]] << 1;
  cpu.PC += 2;
}

void fn9XY0(void **input)
{
  assert(input != NULL);
  uint8_t **input_array = (uint8_t**)input;
  fprintf(log_file, "Skipping next instruct if VX != VY\n");
  if (cpu.regs[*input_array[0]] != cpu.regs[*input_array[1]]) {
    cpu.PC += 4;
  } 
  else {
    cpu.PC += 2;
  }
}

void fnANNN(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  fprintf(log_file, "Setting I to %x\n", *input_array[0]);
  cpu.I = *input_array[0];
  cpu.PC += 2;
}

void fnBNNN(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  fprintf(log_file, "Jumping to %x + V0\n", *input_array[1]);
  cpu.PC= *input_array[0] + cpu.regs[V0];
}

void fnCXNN(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  fprintf(log_file, "Setting VX to random number & NN\n");
  cpu.regs[*input_array[0]] = rand() & *input_array[1]; 
  cpu.PC += 2; 
}

void fnDXYN(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  fprintf(log_file, "Sprite displayed at %x X %x\n", 
          cpu.regs[*input_array[0]], cpu.regs[*input_array[1]]);
  cpu.locx = cpu.regs[*input_array[0]];
  cpu.locy = cpu.regs[*input_array[1]];
  cpu.height = *input_array[2];
  cpu.regs[VF] = 0;
  
  unsigned short pixel;
	for (int yline=0; yline < cpu.height; yline++){
	    pixel = cpu.mem[cpu.I + yline];
	    for(int xline=0; xline < 8; xline++){
	      if ((pixel & (0x80 >> xline)) != 0){
		if (cpu.gfx[(cpu.locx + xline + ((cpu.locy + yline) * 64))] == 1)
		  cpu.regs[VF] = 1;
		cpu.gfx[cpu.locx + xline + ((cpu.locy + yline) * 64)] ^= 1;
	      }
	    }
      }
  DRAW = 1;
  cpu.PC += 2;
}

void fnEX9E(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  fprintf(log_file, " Skips the next instruction if the key stored in VX is pressed\n");
  if(cpu.keys[cpu.regs[*input_array[0]]] != 0) cpu.PC += 4;
  else cpu.PC += 2;
}

void fnEXA1(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  fprintf(log_file, "Skips the next instruction if the key stored in VX isn't pressed\n");
  fprintf(log_file, "The key is %x\n", (uint8_t)*input_array[0]);
  if(cpu.keys[cpu.regs[*input_array[0]]] == 0) cpu.PC += 4;
  else cpu.PC += 2;

}

void fnFX07(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  fprintf(log_file, "Setting VX to the value of the delay timer\n");
  cpu.regs[*input_array[0]] = cpu.delay_timer;  
  cpu.PC += 2;
}

void fnFX0A(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  fprintf(log_file, "Awaiting key press\n");
  char keyPress = 0;
  /*Took this from the multigesture tutorial*/
  for(int i = 0; i < 16; ++i)
					{
						if(cpu.keys[i] != 0)
						{
							cpu.regs[*input_array[0]] = i;
							keyPress = 1;
						}
					}

					// If we didn't received a keypress, skip this cycle and try again.
					if(!keyPress)						
						return;
  cpu.PC += 2;
}

void fnFX15(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  fprintf(log_file, "Setting delay timer\n");
  cpu.delay_timer = cpu.regs[*input_array[0]];
  cpu.PC += 2;
}

void fnFX18(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  fprintf(log_file, "Setting sound timer\n");
  cpu.sound_timer = cpu.regs[*input_array[0]];
  cpu.PC += 2;
}

void fnFX1E(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  fprintf(log_file, "Adds VX to cpu.I\n");
  cpu.I += cpu.regs[*input_array[0]];
  cpu.PC += 2;
}

void fnFX29(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  fprintf(log_file, "Setting cpu.I to location of cpu.SPrite character in VX\n"); 
  cpu.I = cpu.regs[*input_array[0]] * 0x5;
  cpu.PC += 2;
}

void fnFX33(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  /*Took this from the MultiGesture tutorial*/
  fprintf(log_file, "We like it tricky%d\n", *input_array[0]);
  cpu.mem[cpu.I] =  (cpu.regs[*input_array[0]] / 100);
  cpu.mem[cpu.I + 1] = ((cpu.regs[*input_array[0]] / 10) % 10);
  cpu.mem[cpu.I + 2] = ((cpu.regs[*input_array[0]] % 100) % 10);
  cpu.PC += 2;
}

void fnFX55(void **input)
{
  assert(input != NULL);
  uint16_t **input_array = (uint16_t**)input;
  fprintf(log_file, "Storing V0 to %d to memory starting at cpu.I\n", *input_array[1]);
  for(int i=0; i < *input_array[0]; i++) {
    cpu.mem[cpu.I+i] = cpu.regs[i];
  }
  cpu.I += *input_array[0] +1;
  cpu.PC += 2;
	
}

void fnFX65(void **input)
{
  assert(input != NULL);
  uint8_t **input_array = (uint8_t**)input;
  fprintf(log_file, "Filling V0 to %d from memory starting at cpu.I\n", (uint8_t)*input_array[1]);
  for(int i=0; i < *input_array[0]; i++) {
    cpu.regs[i] = cpu.mem[cpu.I+i];
  }
  cpu.I += *input_array[0] +1;
  cpu.PC += 2;

}
